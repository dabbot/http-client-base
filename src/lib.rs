#![feature(async_await, await_macro, futures_api)]

pub extern crate bytes;
pub extern crate futures;
pub extern crate http;
pub extern crate reqwest;

pub mod prelude;

use bytes::Buf;
use futures::{
    compat::Stream01CompatExt,
    stream::{StreamExt, TryStreamExt},
};
use http::header::{HeaderMap, HeaderName, HeaderValue, InvalidHeaderValue};
use reqwest::{
    r#async::Response,
    Error as ReqwestError,
    Url,
    UrlError,
};

pub type Headers = HeaderMap<HeaderValue>;

pub async fn response_bytes<T>(resp: Response) -> Result<Vec<u8>, T>
    where T: From<ReqwestError> {
    let chunks = await!(resp
        .into_body()
        .compat()
        .err_into::<T>()
        .collect::<Vec<_>>());
    let mut chunk_bytes: Vec<Vec<u8>> = Vec::with_capacity(chunks.len());

    for chunk in chunks {
        chunk_bytes.push(chunk?.collect());
    }

    let total = chunk_bytes.iter().fold(0, |total, chunk| total + chunk.len());

    let mut buf = Vec::with_capacity(total);

    for bytes in chunk_bytes {
        buf.extend(&bytes);
    }

    Ok(buf)
}

pub fn auth_header(
    auth: impl AsRef<str>,
) -> Result<HeaderMap<HeaderValue>, InvalidHeaderValue> {
    _auth_header(auth.as_ref())
}

fn _auth_header(
    auth: &str,
) -> Result<HeaderMap<HeaderValue>, InvalidHeaderValue> {
    let mut headers = HeaderMap::with_capacity(1);
    headers.insert("Authorization", HeaderValue::from_str(auth)?);

    Ok(headers)
}

pub fn headers(
    headers: Vec<(HeaderName, impl Into<HeaderValue>)>,
) -> Result<Headers, InvalidHeaderValue> {
    let mut map = HeaderMap::with_capacity(headers.len());

    for (k, v) in headers {
        map.insert(k, v.into());
    }

    Ok(map)
}

pub fn url<'a>(
    https: bool,
    host: impl AsRef<str>,
    path: impl AsRef<str>,
    params: impl Into<Option<&'a [(&'a str, &'a str)]>>,
) -> Result<Url, UrlError> {
    _url(https, host.as_ref(), path.as_ref(), params.into())
}

fn _url(
    https: bool,
    host: &str,
    path: &str,
    params: Option<&[(&str, &str)]>,
) -> Result<Url, UrlError> {
    let protocol = if https {
        "s"
    } else {
        ""
    };

    let url = format!("http{}://{}{}", protocol, host, path);

    if let Some(params) = params {
        Url::parse_with_params(&url, params)
    } else {
        Url::parse(&url)
    }
}
